Required:
	NET 6.0
	Node.js (npm & npx)
	React
	MSSqlServer

**NOTES**
	--For change the DB Access credentials edit parameters in the "./Backend/CsharpAPI/Controllers/connectionToSQLServer.cs" file.

**BUILD THE PROJECT**

--Building the C# project:
	"cd ./Backend/CsharpAPI" and "dotnet run" to compile the C# REST API and start Backend server.

--Building React Project:
	"cd ./React_Src/gestapp" and "npm install" and "npm start" to install all React modules and start Frontend server.



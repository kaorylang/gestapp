import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Login from './Screens/Login';
import Menu from './Screens/Menu';
import Client_And_Provider from './Screens/Client_And_Provider_Register';
import Transaction_Register from './Screens/Transaction_Register';
import Transaction_Logs from './Screens/Transaction_Logs_Screen';
import Configuration from './Screens/Configuration_Screen';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(<Login />, document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

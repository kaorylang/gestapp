function backBtnHandler(target) {
	target.setState({goMenu: true});
}

export default function BackBtn(props) {
	return <div id="backBtn" onClick={() => backBtnHandler(props.target)}>&laquo;</div>;
}

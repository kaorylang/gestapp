import React from 'react';
import '../Styles/menu_screen_styles.css';

import Client_And_Provider from './Client_And_Provider_Register';
import Transanction_Logs from './Transaction_Logs_Screen';
import Transaction_Register from './Transaction_Register';
import Configuration from './Configuration_Screen';

class Menu extends React.Component {
	constructor(props) {
		super(props);

		this.state = {screenRedirect: 0};
		
		this.onClickBtnHandler = this.onClickBtnHandler.bind(this);
	}

	onClickBtnHandler(e) {
		if(e.target.innerText === "Client Register") this.setState({screenRedirect: 1})
		else if(e.target.innerText === "Provider Register") this.setState({screenRedirect: 2})
		else if(e.target.innerText === "Transaction Register") this.setState({screenRedirect: 3})
		else if(e.target.innerText === "Transaction Logs") this.setState({screenRedirect: 4})
		else this.setState({screenRedirect: 5})
	}

    render() {
		if(this.state.screenRedirect === 1) return <Client_And_Provider screen="client" />;
		else if(this.state.screenRedirect === 2) return <Client_And_Provider screen="provider" />;
		else if(this.state.screenRedirect === 3) return <Transaction_Register />;
		else if(this.state.screenRedirect === 4) return <Transanction_Logs />;
		else if(this.state.screenRedirect === 5) return <Configuration />;

        return (
			<div id="menu-screen-container">
				<button class="menu-screen-button" onClick={this.onClickBtnHandler}>
					<img src={require("../Statics/client_&_provider_register_icon.png")} alt="" />
					<p>Client Register</p>
				</button>
				<button class="menu-screen-button menu-provider-btn" onClick={this.onClickBtnHandler}>
					<img src={require("../Statics/client_&_provider_register_icon.png")} alt="" />
					<p>Provider Register</p>
				</button>
				<button class="menu-screen-button transactionAndLogs-menu-btn" onClick={this.onClickBtnHandler}>
					<img src={require("../Statics/transaction_register_icon.png")} alt="" />
					<p>Transaction Register</p>
				</button>
				<button class="menu-screen-button transactionAndLogs-menu-btn" onClick={this.onClickBtnHandler}>
					<img src={require("../Statics/transaction_logs_icon.png")} alt="" />
					<p>Transaction Logs</p>
				</button>
				<button id="menu-screen-configBtn" onClick={this.onClickBtnHandler}>
					<img src={require("../Statics/config_icon.png")} alt="" />
				</button>
			</div>
		)
    }
}

export default Menu;

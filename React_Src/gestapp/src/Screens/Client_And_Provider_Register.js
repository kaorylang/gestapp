import React from 'react';
import '../Styles/client_and_provider_register_styles.css';
import BackBtn from '../Components/BackBtn';
import Menu from './Menu';
import url from '../Components/APIUrl';

class Client_And_Provider extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            goMenu: false,
            requestMessage: "",
        };

        this.visible = (props.screen === "client") ? true : false;

        this.registerBtnHandler = this.registerBtnHandler.bind(this);
    }

    registerBtnHandler() {
        const inputs = document.getElementById("cliprov-screen-form").childNodes;
        console.log(inputs[0].value.length);

        fetch(url + `provclie?ID_Card=${inputs[0].value}
            &name="${inputs[1].value}"
            &lastName="${this.visible ? inputs[2].value : ""}"
            &email="${this.visible ? inputs[3].value : inputs[2].value}"
            &provOrClient=${this.visible}`
            .replaceAll("\n", "")
            .replaceAll(" ", ""))
            .then((response) => response.json())
            .then((data) => {
                if(data === 200) {
                    this.setState({requestMessage: "ALL OK"});
                    document.getElementById("request-info").style.color = "green";
                    inputs.forEach(curr => curr.value = "");
                } else if( data === 2627) {
                    this.setState({requestMessage: "INVALID Identification Card, Repeated"});
                    document.getElementById("request-info").style.color = "red";
                } else if(data === 9491) {
                    this.setState({requestMessage: <><p>Bad ID Card Format: xxxxxxxxxxx only numbers</p>
                                                    <p>Min = 11 digits & Max = 11 digits</p></>});
                    document.getElementById("request-info").style.color = "red";
                } else if(data === 9492) {
                    this.setState({requestMessage: "ID Card Only Numbers"});
                    document.getElementById("request-info").style.color = "red";

                }
                else {
                    this.setState({requestMessage: "SERVER ERROR"});
                    document.getElementById("request-info").style.color = "red";
                }
            })
            .catch(() => {
                this.setState({requestMessage: <><p>Error Resgistering, check the entered values</p>
                                                <p>All values are required</p></>});
                document.getElementById("request-info").style.color = "red";

            });
    }

    render() {
        if(this.state.goMenu) return <Menu />;

        return (
            <div id="cliprov-screen-container">
                <BackBtn target={this} />

                {(this.visible) ? <h1 id="cliprov-screen-title">CLIENT REGISTER</h1>
                : <h1 id="cliprov-screen-title">PROVIDER REGISTER</h1>}

                <form id="cliprov-screen-form">
                    <input className="cliprov-screen-input" type="text" placeholder="ID Card"/>
                    <input className="cliprov-screen-input" type="text" placeholder="name"/>
                    {(this.visible) ? 
                        <input className="cliprov-screen-input" type="text" placeholder="lastName"/> :
                        null}
                    <input className="cliprov-screen-input" type="text" placeholder="email"/>
                </form>

                <div id="request-info">{this.state.requestMessage}</div>

				<button id="register_CliProv_Btn" onClick={this.registerBtnHandler}>Register</button>
            </div>
        )
    }
}

export default Client_And_Provider;

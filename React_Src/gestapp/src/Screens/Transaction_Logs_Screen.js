import React from 'react';
import '../Styles/transaction_logs_screen_styles.css';
import List from '../Components/List';
import BackBtn from '../Components/BackBtn';
import Menu from '../Screens/Menu';
import url from '../Components/APIUrl';

class Transaction_Logs extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
			goMenu: false,
            jsonData: undefined,
            lastTarget: null 
		};

		this.getJson();

        this.detailsBtnHandler = this.detailsBtnHandler.bind(this);
    }

    detailsBtnHandler(e) {
        if(this.state.lastTarget) this.state.lastTarget.style.height = "30px";
        this.setState({lastTarget: e.target});
        e.target.style.height = "175px";
    }

	getJson() {
		fetch(url + "logs")
		.then(response => response.json())
        .then(data => this.setState({
            jsonData: <List messageTemplate="logs" jsonData={data} btnAction={this.detailsBtnHandler} />
        }));
	}

    render() {
        if(this.state.goMenu) return <Menu />;

        return (
            <div id="transaction-logs-screen-container">
                <BackBtn target={this} />

                <h1>TRANSACTION LOGS</h1>

                <div id="transaction-logs-screen-subcontainer">
                {this.state.jsonData}
                </div>
            </div>
        )
    }
}

export default Transaction_Logs;

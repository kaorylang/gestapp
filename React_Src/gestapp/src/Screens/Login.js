import React from 'react';
import '../Styles/login_screen_styles.css';
import url from '../Components/APIUrl';
import Menu from './Menu';

class Login extends React.Component {
	constructor(props) {
        super(props);
		this.btnAction = this.btnAction.bind(this);

        this.state = {userInput: "", passwordInput: "", validCredentials: false};

        this.userInputHandler = this.userInputHandler.bind(this);
        this.passwordInputHandler = this.passwordInputHandler.bind(this);
	}

	btnAction() {
        fetch(url + `login?userInput='${this.state.userInput}'&passwordInput='${this.state.passwordInput}'`)
        .then((response) => response.json())
        .then((data) => this.setState({validCredentials: data}));
	}

    userInputHandler(e) {
        this.setState({userInput: e.target.value});
    }

    passwordInputHandler(e) {
        this.setState({passwordInput: e.target.value});
    }

    render() {
        if(this.state.validCredentials) return <Menu />

        return (
            <div id="main-container">
                <div id="logo-container">
                    <img src={require("../Statics/warehouse_logo.png")} alt=""
                    id="login-gestapp-logo" />
                </div>

                <div id="form-container">
                    <div id="form-subcontainer">
						<form id="form-login">
							<label id="login-label">LOGIN</label>
							<input type="text" placeholder="User Name" class="input-login" onChange={this.userInputHandler} />
							<input type="text" placeholder="Password" class="input-login" onChange={this.passwordInputHandler} />
							
							<input type="button" value="Login" id="login-btn"
							onClick={this.btnAction} class="input-login" />
						</form>
					</div>
                </div>
            </div>
        )
    }
}

export default Login;

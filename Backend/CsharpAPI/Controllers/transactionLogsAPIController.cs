using System;
using System.Data.SqlClient;
using Controllers.connectionToSQLServer;
using System.Text.Json;

namespace Controllers.transactionLogsAPIController;

class TransactionLogsAPIController {
	public static string listLogs() {
		SqlConnection connection = ConnectionToSQLServer.doConnection(); 
		string query = "SELECT * FROM Invoice";
		SqlCommand command = new SqlCommand(query, connection);
		SqlDataReader reader = command.ExecuteReader();

		List<object> list = new List<object>();

		while(reader.Read()) {
			object invoice;

			if(reader.IsDBNull(1)) {
				invoice = new {
					invId = reader.GetInt32(0),
					invProv = reader.GetString(2),
					invDate = reader.GetDateTime(3).ToString("dd/MM/yyyy"),
					invOperation2 = true,
					invPackaging = reader.GetInt32(5)
				};
			}else {
				invoice = new {
					invId = reader.GetInt32(0),
					invCli = reader.GetString(1),
					invDate = reader.GetDateTime(3).ToString("dd/MM/yyyy"),
					invOperation2 = false,
					invPackaging = reader.GetInt32(5)
				};
			}

			list.Add(invoice);
		}

		return JsonSerializer.Serialize(list);
	}
}

using System;
using System.Data.SqlClient;
using Controllers.connectionToSQLServer;
using System.Text;
using Controllers.rackUbicationLibrary;

namespace Controllers.transactionRegisterAPIController;

// Errors list
enum Error {
	noError,
	unrecognized,
	packagingNameNull,
	inexistentPackagingID,
	priceNull,
	providerIDNull,
	clientIDNull,
	packagingSizeNull,
	categoryNull,
	quantityNull,

	IDCardDoesntExist,
	NoSpaceAvalibleInWarehoses,
};

class transactionRegisterAPIController {
	public static int register(
			bool operation,
			string packagingName,
			int quantity,
			int price,
			string? providerID,
			string? packagingSize,
			string? category,
			string? cliID) {

		Console.WriteLine(operation);
		Console.WriteLine(packagingName);
		Console.WriteLine(quantity);
		Console.WriteLine(price);
		Console.WriteLine(providerID);
		Console.WriteLine(packagingSize);
		Console.WriteLine(category);
		Console.WriteLine(cliID);

		try {
			SqlConnection connection = ConnectionToSQLServer.doConnection();

			string query;
			StringBuilder sb = new StringBuilder();

			if(operation) {
				long widthX, widthY, height;
				widthX = 0;
				widthY = 0;
				height = 0;

				// Errors Deal
				if(price <= 0) return (int) Error.priceNull;
				else if(packagingName == "") return (int) Error.packagingNameNull;
				else if(quantity <= 0) return (int) Error.quantityNull;
				else if(providerID.Length < 11 || providerID.Length > 11) return (int) Error.providerIDNull;
				else if(category == "") return (int) Error.categoryNull;

				if(providerID.Length == 11) {
					try {
						Convert.ToInt64(providerID);
					}catch(Exception e) {
						return (int) Error.providerIDNull;
					}
				}

				if(packagingSize != "") {
					String[] splited = packagingSize.Split('-');

					try {
						widthX = Convert.ToInt64(splited[0]);
						widthY = Convert.ToInt64(splited[1]);
						height = Convert.ToInt64(splited[2]);
					}catch (Exception e) {
						return (int) Error.packagingSizeNull;
					}
				}

				// Record if prov or cli ID exist
				query = $"SELECT * FROM Provider WHERE Prov_IdentificationCard = {providerID}";
				SqlCommand command = new SqlCommand(query, connection);
				SqlDataReader reader = command.ExecuteReader();

				if(reader.Read()) {
					reader.Close();
					
					// Rack Finder
					int rackUbication = RackUbicationLibrary.getUbication(widthY * widthX * height);
					if(rackUbication < 0) return (int) Error.NoSpaceAvalibleInWarehoses;

					sb.Append($"INSERT INTO Packaging VALUES('{packagingName}', '{quantity}', '{category}', '{price}', ");
					sb.Append($"'{packagingSize}', {rackUbication}, '{providerID}')");
					query = sb.ToString();

					command = new SqlCommand(query, connection);
					command.ExecuteNonQuery();

					// Record in Invoice
					string date = DateTime.Now.ToString("yyyy/MM/dd");

					query = $"SELECT TOP 1 Pack_ID FROM Packaging ORDER BY Pack_ID DESC";
					command = new SqlCommand(query, connection);
					reader = command.ExecuteReader();
					reader.Read();
					int lastPackaging = reader.GetInt32(0);
					reader.Close();

					Console.WriteLine(date);

					sb.Clear();
					sb.Append($"INSERT INTO Invoice (Inv_Prov, Inv_Date, Inv_Operation, Inv_Packaging)");
					sb.Append($"VALUES ('{providerID}', CONVERT(DATETIME, '{date}'), 'i', {lastPackaging})");
					query = sb.ToString();
					command = new SqlCommand(query, connection);
					command.ExecuteNonQuery();
				}else return (int) Error.IDCardDoesntExist;
				Console.WriteLine("All Ok");
			}else {
				// Error Deal
				long packID;
				if(packagingName == "") return (int) Error.inexistentPackagingID;
				else if(cliID.Length < 11 || cliID.Length > 11) return (int) Error.clientIDNull;
				else if(quantity <= 0) return (int) Error.quantityNull;

				try {
					packID = Convert.ToInt64(packagingName);
				}
				catch(Exception e) {
					return (int) Error.inexistentPackagingID;
				}
				
				if(cliID.Length == 11) {
					try {
						Convert.ToInt64(cliID);
					}catch(Exception e) {
						return (int) Error.clientIDNull;
					}
				}

				query = $"SELECT * FROM Client WHERE Cli_IdentificationCard = {cliID}";
				SqlCommand command = new SqlCommand(query, connection);
				SqlDataReader reader = command.ExecuteReader();

				if(reader.Read()) {
					reader.Close();

					// Rack Volume Increase
					int volume = 0;
					int rackUbication = 0;
					int freeSpace = 0;

					query = $"SELECT Pack_Size, Pack_Ubication FROM Packaging WHERE Pack_ID = {packID}";
					command = new SqlCommand(query, connection);
					reader = command.ExecuteReader();

					if(reader.Read()) {
						string[] size = reader.GetString(0).Split('-');
						volume = Convert.ToInt32(size[0]) * Convert.ToInt32(size[1]) * Convert.ToInt32(size[2]);
						rackUbication = reader.GetInt32(1);
						reader.Close();

						query = $"SELECT Rack_FreeSpace FROM Racks WHERE Rack_ID = {rackUbication}";
						command = new SqlCommand(query, connection);
						reader = command.ExecuteReader();
						reader.Read();
						freeSpace = reader.GetInt32(0);
						reader.Close();
					}else return (int) Error.inexistentPackagingID;

					query = $"UPDATE Racks SET Rack_FreeSpace = {freeSpace + volume} WHERE Rack_ID = {rackUbication}";
					command = new SqlCommand(query, connection);
					command.ExecuteNonQuery();

					// Record in Invoice
					string date = DateTime.Now.ToString("yyyy/MM/dd");

					sb.Append($"INSERT INTO Invoice (Inv_Cli, Inv_Date, Inv_Operation, Inv_Packaging)");
					sb.Append($"VALUES ('{cliID}', CONVERT(DATETIME, '{date}'), 'o', {packID})");
					query = sb.ToString();
					command = new SqlCommand(query, connection);
					command.ExecuteNonQuery();

					Console.WriteLine("Ok");
				}else return (int) Error.clientIDNull;

				Console.WriteLine("no");
			}

			sb.Clear();
			connection.Close();
			return (int) Error.noError;
		}catch(SqlException e) {
			return (int) Error.unrecognized;
		}
	}
}


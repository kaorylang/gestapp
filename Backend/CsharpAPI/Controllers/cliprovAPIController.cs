using System;
using System.Data.SqlClient;
using Controllers.connectionToSQLServer;

namespace Controllers.cliprovAPIController;

class CliprovAPIController {
	public static int validate_data(
			string ID_Card,
			string name,
			string lastName,
			string email,
			bool provOrClient)
	{
		//ID_Card length control
		if(ID_Card.Length > 11 || ID_Card.Length < 11) return 9491;

		//ID_Card only numbers control
		try {
			long numbero = Int64.Parse(ID_Card);
		} catch (Exception e) {
			return 9492;
		}

		try {
			SqlConnection connection = ConnectionToSQLServer.doConnection();
			string query = "";

			if(provOrClient) {
				query = $"INSERT INTO Client VALUES ('{ID_Card}','{name}','{lastName}','{email}')";
			} else {
				query = $"INSERT INTO Provider VALUES ('{ID_Card}','{name}','{email}')";
			}

			SqlCommand command = new SqlCommand(query, connection);
			command.ExecuteNonQuery();
			Console.WriteLine("Query done");
			Console.WriteLine("----------");
			connection.Close();
			return 200;

		} catch (SqlException e) {
			return e.Number;
		}
	}
}

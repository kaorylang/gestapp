using System;
using Controllers.usersAPIController;
using Controllers.cliprovAPIController;
using Controllers.configAPIController;
using Controllers.transactionRegisterAPIController;
using Controllers.transactionLogsAPIController;
using Controllers.rackUbicationLibrary;

namespace Program;

class Program {
	static void Main(string[] args) {
		Console.WriteLine("Hello World");
		
		var builder = WebApplication.CreateBuilder(args);

		string policy = "MyPolicy";
		builder.Services.AddCors(options => {
				options.AddPolicy(name: policy, build => {
					build.SetIsOriginAllowed(origin => new Uri(origin).Host == "localhost")
					.AllowAnyHeader().AllowAnyMethod();
				});
		});

		var app = builder.Build();
		app.UseCors(policy);

		app.MapGet("/", () => "Hello World!");
		app.MapGet("/login", UsersAPIController.validate_credentials);
		app.MapGet("/provclie", CliprovAPIController.validate_data);

		app.MapGet("/config/addWarehouse", ConfigAPIController.addWarehouse);
		app.MapGet("/config/addRack", ConfigAPIController.addRack);
		app.MapGet("/config/listWarehouse", ConfigAPIController.listWarehouse);
		app.MapGet("/config/listRacks", ConfigAPIController.listRacks);

		app.MapGet("/register", transactionRegisterAPIController.register);
		app.MapGet("/logs", TransactionLogsAPIController.listLogs);
		app.MapGet("/ubication", RackUbicationLibrary.showUbication);

		app.Run();
	}
}

USE GestAppDB;

CREATE TABLE Users (
	User_Name VARCHAR(100) NOT NULL,
	User_Password VARCHAR(100) NOT NULL
);

INSERT INTO Users VALUES ('admin', 'admin');

CREATE TABLE Client (
	Cli_IdentificationCard VARCHAR(11) PRIMARY KEY NOT NULL,
	Cli_Name VARCHAR(100) NOT NULL,
	Cli_LastName VARCHAR(100) NOT NULL,
	Cli_Email VARCHAR(100) NOT NULL
);

CREATE TABLE Provider (
	Prov_IdentificationCard VARCHAR(11) PRIMARY KEY NOT NULL,
	Prov_Name VARCHAR(100) NOT NULL,
	Prov_Email VARCHAR(100) NOT NULL
);

INSERT INTO Provider VALUES ('12345678901', 'Jose', 'jose@email.com');

CREATE TABLE Warehouse (
	Whse_ID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Whse_Ubication VARCHAR(100)
)

CREATE TABLE Racks (
	Rack_ID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Rack_Warehouse INT NOT NULL REFERENCES Warehouse(Whse_ID),
	Rack_HeightCms INT NOT NULL,
	Rack_XWidthCms INT NOT NULL,
	Rack_YWidthCms INT NOT NULL,
	Rack_FreeSpace INT NOT NULL
)

CREATE TABLE Packaging (
	Pack_ID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Pack_Name VARCHAR(100) NOT NULL,
	Pack_Quantity VARCHAR(100) NOT NULL,
	Pack_Category CHAR NOT NULL,
	Pack_Price INT NOT NULL,
	Pack_Size VARCHAR(100) NOT NULL,
	Pack_Ubication INT NOT NULL REFERENCES Racks(Rack_ID),
	Pack_Prov VARCHAR(11) NOT NULL REFERENCES Provider(Prov_IdentificationCard)
)

CREATE TABLE Invoice (
	Inv_ID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Inv_Cli VARCHAR(11) REFERENCES Client(Cli_IdentificationCard),
	Inv_Prov VARCHAR(11) REFERENCES Provider(Prov_IdentificationCard),
	Inv_Date DATE NOT NULL,
	Inv_Operation CHAR NOT NULL,
	Inv_Packaging INT NOT NULL REFERENCES Packaging(Pack_ID)
)

